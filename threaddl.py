#!/usr/bin/python

import json
import requests
import argparse
import os

def parse_args():
    parser = argparse.ArgumentParser(description='Downloads images from a 4chan thread.')
    parser.add_argument('url',type=str, help='url of the thread to download images from')
    parser.add_argument('-d', '--dir', nargs=1, type=str, help='directory in which to save the files')
    parser.add_argument('-e', '--ext', nargs=1, type=str, help='specify which extentions to download')
    return parser.parse_args()

def get_posts(board, thread):
    r = requests.get(api_url(board, thread))
    return json.loads(r.text)["posts"]

def get_board(url):
    return url.split('/')[3]

def get_thread(url):
    return url.split('/')[5]

def api_url(board, thread_number):
    return "https://a.4cdn.org/" + board + "/thread/" + thread_number + ".json"

def file_url(board, time, ext):
    return "https://i.4cdn.org/" + board + "/" + str(time) + ext

def get_data(board, thread, exts, posts):
    data = {
            'urls':[],
            'names':[]}

    for post in posts:
        if("ext" in post):
            if(post["ext"] in exts):
                data['urls'].append(file_url(board,post["tim"],post["ext"]))
                data['names'].append(str(post['tim']) + post['ext'])

    return data

def chdir(path):
    if(os.path.exists(path) and os.path.isdir(path)):
        os.chdir(path)
    
    else:
        os.makedirs(path)
        os.chdir(path)

def save_file(filename, url, chunksize=15):
    r = requests.get(url, stream=True)

    with open(filename, 'wb') as fd:
        for chunk  in r.iter_content(chunksize):
            fd.write(chunk)

args = parse_args()

if args.dir:
    chdir(args.dir[0])

if args.ext:
    exts = args.ext[0].split(',')

else:
    exts = ['.jpg','.png','.gif','.webm']

board = get_board(args.url)
thread = get_thread(args.url)
data = get_data(board, thread, exts, get_posts(board, thread))

for url,name in zip(data['urls'],data['names']):
    print("saving file " + name)
    save_file(name, url)

print("Done")
